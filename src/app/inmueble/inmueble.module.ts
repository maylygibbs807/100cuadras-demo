import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InmuebleRoutingModule } from './inmueble-routing.module';
import { InmuebleComponent } from './inmueble.component';
import { InmuebleService } from '../services/inmueble/inmueble.service';

@NgModule({
  declarations: [InmuebleComponent],
  imports: [
    CommonModule,
    InmuebleRoutingModule
  ],
  providers: [InmuebleService]
})
export class InmuebleModule { }
