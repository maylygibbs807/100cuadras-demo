import { Component, OnInit } from '@angular/core';
import { InmuebleService } from '../services/inmueble/inmueble.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-inmueble',
  templateUrl: './inmueble.component.html',
  styleUrls: ['./inmueble.component.scss']
})
export class InmuebleComponent implements OnInit {

  inmuebles: any;
  constructor(public route: ActivatedRoute, private inmuebleService: InmuebleService) { }

  ngOnInit() {

    const routeParams = this.route.snapshot.params;
    this.inmuebleService.getInmuebles(routeParams.tipoTransaccion, routeParams.ciudad, routeParams.tipoInmueble).subscribe(
      resp => {
        this.inmuebles = resp;
    },
    error => {
      console.log(error);
    });

  }






}