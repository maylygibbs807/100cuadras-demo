import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';

@Injectable()
export class InmuebleService {


  constructor(private api: ApiService) { }

  getInmuebles(tipoTransaccion, ciudad, tipoInmueble) {
    return this.api.get('inmuebles');
  }
}
