import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: './landing/landing.module#LandingModule'
  },
  {
    path: ':tipoTrasaccion',
    loadChildren: './inmueble/inmueble.module#InmuebleModule'
  },
  {
    path: ':tipoTrasaccion/:ciudad/:tipoInmueble',
    loadChildren: './inmueble/inmueble.module#InmuebleModule'
  },
  {
    path: ':tipoTrasaccion/:ciudad/:localidad/:tipoInmueble',
    loadChildren: './inmueble/inmueble.module#InmuebleModule'
  },
  {
    path: ':tipoTrasaccion/:ciudad/:localidad/:barrio/:tipoInmueble',
    loadChildren: './inmueble/inmueble.module#InmuebleModule'
  },
  {
    path: ':tipoTrasaccion/:tipoInmueble',
    loadChildren: './inmueble/inmueble.module#InmuebleModule'
  },
  {
    path: ':tipoTrasaccion/:departamento/:ciudad/:tipoInmueble',
    loadChildren: './inmueble/inmueble.module#InmuebleModule'
  },
  {
    path: ':tipoTrasaccion/:departamento/:ciudad/:localidad/:tipoInmueble',
    loadChildren: './inmueble/inmueble.module#InmuebleModule'
  },
  {
    path: ':tipoTrasaccion/:departamento/:ciudad/:localidad/:barrio/:tipoInmueble',
    loadChildren: './inmueble/inmueble.module#InmuebleModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
