import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-searchengine',
  templateUrl: './searchengine.component.html',
  styleUrls: ['./searchengine.component.scss']
})
export class SearchengineComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  load_inmuebles_arriendo(tipoTransaccion, tipoInmueble) {
    this.router.navigate([tipoTransaccion, tipoInmueble]);
  }

  load_inmuebles_venta(tipoTransaccion, tipoInmueble) {
    this.router.navigate([tipoTransaccion, tipoInmueble]);
  }

  load_inmuebles_arriendo_bogota(tipoTransaccion, ciudad, tipoInmueble) {
    this.router.navigate([tipoTransaccion, ciudad, tipoInmueble]);
  }

}
