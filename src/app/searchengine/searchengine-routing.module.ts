import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchengineComponent } from './searchengine.component';

const routes: Routes = [
  {path: '', component: SearchengineComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchengineRoutingModule { }
