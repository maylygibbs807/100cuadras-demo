import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchengineRoutingModule } from './searchengine-routing.module';
import { SearchengineComponent } from './searchengine.component';

@NgModule({
  declarations: [SearchengineComponent],
  imports: [
    CommonModule,
    SearchengineRoutingModule
  ]
})
export class SearchengineModule { }
